#ifndef CMAINSCREEN_H
#define CMAINSCREEN_H
//#include <QWidget>
#include "ui_CMainScreen.h"
#include "EModule.h"

class CMainScreen : public QWidget, private Ui::CMainScreen
{
    Q_OBJECT

private:
    EModule activeModule = EModule::None;

public:
    explicit CMainScreen(QWidget *parent = 0);
    EModule getActiveModule();

signals:
    void signalRequestModuleChange(EModule);
    void signalVolumeChange(int lvl);

public slots:
    void slotSetActiveModule(QWidget *screen, EModule module);

private slots:
    void on_pushButtonVideo_clicked();
    void on_pushButtonRadio_clicked();
    void on_pushButtonInfo_clicked();
    void on_sliderVolume_sliderReleased();
    void on_sliderVolume_valueChanged(int value);
    void on_sliderVolume_sliderMoved(int position);
    void on_pushButtonPlayer_clicked();
    void on_pushButtonSettings_clicked();
    void on_pushButtonNavi_clicked();
};

#endif // CMAINSCREEN_H
