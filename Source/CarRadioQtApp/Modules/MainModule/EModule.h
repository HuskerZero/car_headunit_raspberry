#ifndef EMODULE_H
#define EMODULE_H


enum EModule
{
    None,
    Dashboard,
    Recorder,
    Radio,
    Player,
    Navi,
    Settings
};

#endif // EMODULE_H
