#ifndef CBASEMODULE_H
#define CBASEMODULE_H

#include <QWidget>
#include <QObject>
#include "EModule.h"

class CBaseModule : public QObject
{
protected:
    QWidget *view = nullptr;
    EModule moduleType = EModule::None;

public:
    virtual void initModule() = 0;
    virtual QWidget* getView() = 0;
    virtual EModule getType() = 0;
};

#endif // CBASEMODULE_H
