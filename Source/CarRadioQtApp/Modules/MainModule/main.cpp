//#include "CMainScreen.h"
#include <QApplication>
#include "CModuleManager.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    CModuleManager *manager = new CModuleManager();

    return a.exec();
}
