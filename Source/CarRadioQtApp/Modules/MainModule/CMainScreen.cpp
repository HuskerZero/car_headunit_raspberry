#include "CMainScreen.h"

CMainScreen::CMainScreen(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
}

EModule CMainScreen::getActiveModule()
{
    return activeModule;
}

void CMainScreen::slotSetActiveModule(QWidget *screen, EModule module)
{
    if(module == activeModule) return;

    activeModule = module;

    if(nullptr != moduleScreen)
    {
        moduleScreen->hide();
        moduleScreen->setParent(nullptr);
    }
    moduleScreen = screen;
    moduleScreen->setParent(this);
    moduleScreen->show();
}

void CMainScreen::on_pushButtonVideo_clicked()
{
    emit signalRequestModuleChange(EModule::Recorder);
}

void CMainScreen::on_pushButtonRadio_clicked()
{
    emit signalRequestModuleChange(EModule::Radio);
}

void CMainScreen::on_pushButtonInfo_clicked()
{
    emit signalRequestModuleChange(EModule::Dashboard);
}

void CMainScreen::on_sliderVolume_sliderReleased()
{
    emit signalVolumeChange(sliderVolume->value());
}

void CMainScreen::on_sliderVolume_sliderMoved(int position)
{
    emit signalVolumeChange(sliderVolume->value());
}

void CMainScreen::on_sliderVolume_valueChanged(int value)
{

}

void CMainScreen::on_pushButtonPlayer_clicked()
{
    emit signalRequestModuleChange(EModule::Player);
}

void CMainScreen::on_pushButtonSettings_clicked()
{
    emit signalRequestModuleChange(EModule::Settings);
}

void CMainScreen::on_pushButtonNavi_clicked()
{
    emit signalRequestModuleChange(EModule::Navi);
}
