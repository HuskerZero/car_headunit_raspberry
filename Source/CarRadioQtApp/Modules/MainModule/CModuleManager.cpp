#include "CModuleManager.h"
#include "CMainScreen.h"
#include <iostream>

CModuleManager::CModuleManager()
{
    system("sudo modprobe bcm2835-v4l2");
    initMainScreen();
    initModules();
    initSignalSlotConnections();

    emit signalChangeActiveScreen(getModule(Recorder)->getView(), Recorder);
    dynamic_cast<CRecorderController*>(getModule(Recorder))->toggleStream();
}

CModuleManager::~CModuleManager()
{
    if(modulesList != nullptr && !modulesList->empty())
    {
        //modulesList->clear();
        for(int i = 0; i < modulesList->size(); i++)
        {
            delete modulesList->at(i);
            modulesList->pop_back();
        }
        delete modulesList;
        modulesList = nullptr;
    }
    if(nullptr != mainScreen)
    {
        delete mainScreen;
        mainScreen = nullptr;
    }

}

void CModuleManager::initMainScreen()
{
    mainScreen = new CMainScreen(nullptr);
    mainScreen->show();
}

void CModuleManager::initModules()
{
    modulesList = new std::vector<CBaseModule*>();
    modulesList->push_back(new CRecorderController());
    modulesList->push_back(new CRadioController());
    modulesList->push_back(new CDashboardController());
    modulesList->push_back(new CPlayerController());
    settings = new CSettingsController();
    modulesList->push_back(settings);
    modulesList->push_back(new CNaviController());
}

void CModuleManager::initSignalSlotConnections()
{
    connect(mainScreen, SIGNAL(signalRequestModuleChange(EModule)), this, SLOT(slotSwitchModuleScreen(EModule)));
    connect(this,SIGNAL(signalChangeActiveScreen(QWidget*, EModule)), mainScreen, SLOT(slotSetActiveModule(QWidget*, EModule)));
    connect(mainScreen, SIGNAL(signalVolumeChange(int)), this, SLOT(slotVolumeChange(int)));
    connect(settings, SIGNAL(signalSwitchModule(EModule)), this, SLOT(slotSwitchModuleScreen(EModule)));
}

CBaseModule *CModuleManager::getModule(EModule mod)
{
    switch(mod)
    {
    case EModule::Player:
        system("amixer set Mic off");
        break;
    case EModule::Radio:
        system("amixer set Mic on");
        break;
    }

    for(int i = 0; i < modulesList->size(); i++)
    {
         if(modulesList->at(i)->getType() == mod)
         {
             return modulesList->at(i);
         }
    }
    return nullptr;
}

void CModuleManager::slotSwitchModuleScreen(EModule moduleName)
{
    emit signalChangeActiveScreen(getModule(moduleName)->getView(), moduleName);
}

void CModuleManager::slotVolumeChange(int lvl)
{
    std::string command = "amixer set Speaker " + std::to_string(lvl) + "%";
    system(command.c_str());
}
