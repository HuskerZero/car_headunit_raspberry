#ifndef CMODULEMANAGER_H
#define CMODULEMANAGER_H

#include <QWidget>
#include <QObject>
#include "EModule.h"
#include <vector>
#include "../RecorderModule/CRecorderController.h"
#include "../RadioModule/CRadioController.h"
#include "../DashboardModule/CDashboardController.h"
#include "../PlayerModule/CPlayerController.h"
#include "../SettingsModule/CSettingsController.h"
#include "../NavigationModule/CNaviController.h"
#include "CBaseModule.h"

class CModuleManager : public QObject
{
    Q_OBJECT

private:
    QWidget *mainScreen = nullptr;
    std::vector<CBaseModule*> *modulesList = nullptr;

public:
    CModuleManager();
    ~CModuleManager();

private:
    void initMainScreen();
    void initModules();
    void initSignalSlotConnections();
    CBaseModule* getModule(EModule mod);
    CSettingsController *settings = nullptr;

public slots:
    void slotSwitchModuleScreen(EModule mod);
    void slotVolumeChange(int lvl);

signals:
    void signalActiveScreenChanged(EModule);
    void signalChangeActiveScreen(QWidget*, EModule);
    void signalStartRecording();


};

#endif // CMODULEMANAGER_H
