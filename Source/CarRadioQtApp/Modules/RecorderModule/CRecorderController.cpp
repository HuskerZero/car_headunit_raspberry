#include "CRecorderController.h"
#include "CRecorderScreen.h"
#include "CVideoProvider.h"
#include <QTimer>
#include <QString>
#include <iostream>

CRecorderController::CRecorderController()
    : CBaseModule()
{
    initModule();
}

CRecorderController::~CRecorderController()
{
    m_oThread->quit();
    m_oThread->wait();
    if(nullptr != view)
    {
        delete view;
        view = nullptr;
    }
    if (nullptr != videoManager)
    {
        delete videoManager;
    }
}

void CRecorderController::initModule()
{
    moduleType = EModule::Recorder;
    view = new CRecorderScreen(nullptr);
    videoManager = new CVideoFileManager();
    setupVideo();
}

QWidget* CRecorderController::getView()
{
    return view;
}

EModule CRecorderController::getType()
{
    return moduleType;
}

void CRecorderController::toggleStream()
{
    emit signalToggleStream();
}

void CRecorderController::setupVideo()
{
    m_oThread = new QThread();
    CVideoProvider *provider = new CVideoProvider();
    QTimer *streamTrigger = new QTimer();
    streamTrigger->setInterval(1000/30);

    connect(provider, SIGNAL(signalSendFrameToRecord(QImage)), videoManager, SLOT(slotAddFrameToFile(QImage)));
    connect(view, SIGNAL(signalSaveVideo()), videoManager, SLOT(saveVideoFile()));

    connect(streamTrigger, SIGNAL(timeout()), provider, SLOT(slotGrabFrame()));
    connect(this, SIGNAL(signalSetup(int)), provider, SLOT(slotSetup(int)));
    connect(this, SIGNAL(signalToggleStream()), provider, SLOT(slotToggleStream()));

    connect(view, SIGNAL(signalToggleStream()), this, SLOT(slotToggleStream()));
    connect(this, SIGNAL(signalToggleStream()), view, SLOT(slotToggleStream()));

    connect(provider, SIGNAL(signalSendFrame(QImage)), this, SLOT(slotGetFrame(QImage)));
    connect(this, SIGNAL(signalShowFrame(QImage)), view, SLOT(slotShowFrame(QImage)));

    connect(m_oThread, SIGNAL(started()), streamTrigger, SLOT(start()));
    connect(m_oThread, SIGNAL(finished()), provider, SLOT(deleteLater()));
    connect(m_oThread, SIGNAL(finished()), streamTrigger, SLOT(deleteLater()));

    provider->moveToThread(m_oThread);
    streamTrigger->moveToThread(m_oThread);
    m_oThread->start();
    std::cout << "attempt to start opencv" << std::endl;
    emit signalSetup(0);
}

void CRecorderController::slotGetFrame(QImage a_oFrame)
{
    emit signalShowFrame(a_oFrame);
}

void CRecorderController::slotToggleStream()
{
    emit signalToggleStream();
}
