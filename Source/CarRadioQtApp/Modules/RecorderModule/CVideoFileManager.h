#ifndef CVIDEOFILEMANAGER_H
#define CVIDEOFILEMANAGER_H

#include <QObject>
#include <opencv2/opencv.hpp>
#include <QQueue>
#include <string>
#include <QImage>

class CVideoFileManager : public QObject
{
    Q_OBJECT
public:
    explicit CVideoFileManager(QObject *parent = nullptr);
    ~CVideoFileManager();
private:
    QQueue<cv::Mat> *videoFrames = nullptr;
    //std::vector<cv::Mat*> *videoFrames = nullptr;
    int secondsBeforeSave = 30;
    int secondsAfterSave = 5;
    int fps = 30;
    bool saveVideo = false;
    int framesToSaveCounter = 0;
    std::string dirPath = "RecVid";

    void saveFile();
    int getMaxFrames();
    void createDir();
    std::string resolveFileName();

signals:

public slots:
    void saveVideoFile();
    void slotAddFrameToFile(QImage frame);

};

#endif // CVIDEOFILEMANAGER_H
