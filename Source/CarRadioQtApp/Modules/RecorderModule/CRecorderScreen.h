#ifndef CRECORDERSCREEN_H
#define CRECORDERSCREEN_H

#include <QWidget>
#include <ui_CRecorderScreen.h>
#include <QImage>

class CRecorderScreen : public QWidget, private Ui::CRecorderScreen
{
    Q_OBJECT

public:
    explicit CRecorderScreen(QWidget *parent = 0); 

private:

signals:
    void signalToggleStream();
    void signalSaveVideo();

private slots:
    void slotShowFrame(QImage frame);
    void slotToggleStream();

    void on_pushButtonStart_clicked();
};

#endif // CRECORDERSCREEN_H
