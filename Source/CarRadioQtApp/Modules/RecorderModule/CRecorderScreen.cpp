#include "CRecorderScreen.h"

CRecorderScreen::CRecorderScreen(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
    videoScreen->setScaledContents(true);
}

void CRecorderScreen::slotShowFrame(QImage frame)
{
    videoScreen->setPixmap(QPixmap::fromImage(frame));
}

void CRecorderScreen::slotToggleStream()
{
   /*if(!pushButtonStart->text().compare("▶"))
    {
        pushButtonStart->setText("||");
    }
    else
    {
        pushButtonStart->setText("▶");
    }*/
}

void CRecorderScreen::on_pushButtonStart_clicked()
{
    emit signalSaveVideo();
}

