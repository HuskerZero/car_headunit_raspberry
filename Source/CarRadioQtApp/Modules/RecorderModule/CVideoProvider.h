#ifndef CVIDEOPROVIDER_H
#define CVIDEOPROVIDER_H

#include <QObject>
#include <QImage>
#include <opencv2/opencv.hpp>
#include <string>
#include <QString>

class CVideoProvider : public QObject
{
    Q_OBJECT

private:
    cv::Mat m_oFrame;

    cv::VideoCapture *m_oVCapture = nullptr;
    bool m_bStatus = false;
    bool m_bToggleStream = false;

    void checkIfDeviceIsAlreadyOpened(const int a_strDevice);
    void processFrame();

public:
    explicit CVideoProvider(QObject *parent = 0);
    ~CVideoProvider();

signals:
    void signalSendFrame(QImage frameProcessed);
    void signalSendFrameToRecord(QImage frame);

public slots:
    void slotGrabFrame();
    void slotSetup(const int a_strDevice);
    void slotToggleStream();
};

#endif // CVIDEOPROVIDER_H
