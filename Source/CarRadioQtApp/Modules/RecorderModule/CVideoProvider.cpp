#include "CVideoProvider.h"
#include <iostream>
#include <QDebug>
#include <string>

void CVideoProvider::checkIfDeviceIsAlreadyOpened(const int a_strDevice)
{
    std::cout << m_oVCapture->isOpened() << std::endl;
    if(m_oVCapture->isOpened())
    {
        m_oVCapture->release();
    }
    std::cout << "opening: " << a_strDevice << std::endl;
    m_oVCapture->open(a_strDevice);
}

void CVideoProvider::processFrame()
{

}

CVideoProvider::CVideoProvider(QObject *parent)
    : QObject(parent),
      m_bStatus(false),
      m_bToggleStream(false)
{
    m_oVCapture = new cv::VideoCapture();
    m_oVCapture->set(cv::CAP_PROP_FRAME_WIDTH, 1920);
    m_oVCapture->set(cv::CAP_PROP_FRAME_HEIGHT, 1080);
}

CVideoProvider::~CVideoProvider()
{
    if(m_oVCapture->isOpened())
    {
        m_oVCapture->release();
    }

    if(nullptr != m_oVCapture)
    {
        delete m_oVCapture;
    }
}

void CVideoProvider::slotGrabFrame()
{
    if(!m_bToggleStream)
    {
        return;
    }

    (*m_oVCapture) >> m_oFrame;
    if(m_oFrame.empty())
    {
        return;
    }

    //processFrame();
    cv::cvtColor(m_oFrame, m_oFrame, cv::COLOR_BGR2GRAY);
    QImage output((const unsigned char *) m_oFrame.data, m_oFrame.cols, m_oFrame.rows, QImage::Format_Indexed8);
    //std::cout << "QImage Size: x: " + std::to_string(output.width()) + " y: " + std::to_string(output.height()) + "\n";
    emit signalSendFrameToRecord(output.copy());
    emit signalSendFrame(output.copy());
}

void CVideoProvider::slotSetup(const int a_strDevice)
{
    checkIfDeviceIsAlreadyOpened(a_strDevice);
    m_bStatus=m_oVCapture->isOpened()?true:false;
}

void CVideoProvider::slotToggleStream()
{
    m_bToggleStream = !m_bToggleStream;
}

