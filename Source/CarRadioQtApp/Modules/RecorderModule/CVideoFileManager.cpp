#include "CVideoFileManager.h"
#include "QDir"
#include "QDateTime"
#include <QDebug>


CVideoFileManager::CVideoFileManager(QObject *parent)
    : QObject(parent)
{
    videoFrames = new QQueue<cv::Mat>();
}

CVideoFileManager::~CVideoFileManager()
{
    if (nullptr != videoFrames)
    {
        while(!videoFrames->isEmpty())
        {
            videoFrames->dequeue();
        }
        delete videoFrames;
        videoFrames = nullptr;
    }
}

void CVideoFileManager::saveFile()
{
    qDebug() << "Trying to save video\n";
    cv::Size frameSize = videoFrames->first().size();
    //std::cout << "Mat Size: x: " + std::to_string(frameSize.width) + " y: " + std::to_string(frameSize.height) + "\n";
    QQueue<cv::Mat> *video = videoFrames;
    videoFrames = new QQueue<cv::Mat>();

    qDebug() << "Create dir\n";
    createDir();
    qDebug() << "Video writer setup\n";
    cv::VideoWriter *writer = new cv::VideoWriter();
    writer->open(/*dirPath + resolveFileName()*/ "test_ras.avi", CV_FOURCC('M','J','P','G'), double(fps), frameSize, false);
    int counter = 0;

    std::cout << "Writer opened?\n";

    if(writer->isOpened())
    {
         std::cout << "True\n";
    }
    else
    {
        std::cout << "False\n";
    }

    qDebug() << "Saving frames\n";
    while(!video->isEmpty())
    {
        //qDebug() << "Dequeue\n";
        writer->write(video->dequeue());
    }
    writer->release();
    saveVideo = false;
    qDebug() << "Saved\n";
}

int CVideoFileManager::getMaxFrames()
{
    return (secondsBeforeSave + secondsAfterSave) * fps;
}

void CVideoFileManager::createDir()
{
    QDir dir;
    QString path = QString::fromStdString(dirPath);
    if(!dir.exists(path))
    {
        dir.mkdir(path);
    }
}

std::string CVideoFileManager::resolveFileName()
{
    return QDateTime::currentDateTime().toString("hh:mm:ss_dd.MM.yyyy").toStdString();
}

void CVideoFileManager::saveVideoFile()
{
    saveVideo = true;
    framesToSaveCounter = secondsAfterSave * fps;
}

void CVideoFileManager::slotAddFrameToFile(QImage frame)
{ 

    cv::Mat frame2 = cv::Mat(frame.height(), frame.width(), CV_8U,
                       const_cast<uchar*>(frame.bits()),
                       frame.bytesPerLine()).clone();

    if(videoFrames->count() >= getMaxFrames() -1)
    {
        //qDebug() << "Delete frame\n";
        videoFrames->dequeue();
    }
    //qDebug() << "Adding frame to file\n";
    videoFrames->enqueue(frame2);

    if(saveVideo && framesToSaveCounter > 0)
    {
        framesToSaveCounter--;
    }
    else if(saveVideo && framesToSaveCounter == 0)
    {
        saveFile();
    }

}
