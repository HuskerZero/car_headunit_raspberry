#ifndef CRECORDERCONTROLLER_H
#define CRECORDERCONTROLLER_H

#include "../MainModule/CBaseModule.h"
#include "CVideoFileManager.h"
#include <QThread>
#include <QString>

class CRecorderController : public CBaseModule
{
    Q_OBJECT

public:
    CRecorderController();
    ~CRecorderController();
    // CBaseModule interface
public:
    virtual void initModule();
    virtual QWidget *getView();
    virtual EModule getType();
    void toggleStream();

private:
    QThread *m_oThread = nullptr;
    CVideoFileManager *videoManager = nullptr;
    void setupVideo();

signals:
    void signalSetup(int a_strDevice);
    void signalToggleStream();
    void signalShowFrame(QImage a_oFrame);

private slots:
    void slotGetFrame(QImage a_oFrame);
    void slotToggleStream();
};

#endif // CRECORDERCONTROLLER_H
