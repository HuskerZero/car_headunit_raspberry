#ifndef CNAVIPROVIDER_H
#define CNAVIPROVIDER_H

#include <QObject>
#include <QString>
#include <QSerialPort>

class CNaviProvider : public QObject
{
    Q_OBJECT
public:
    explicit CNaviProvider(QObject *parent = nullptr);

private:
    void initNavi();
    void openSerial();
    void closeSerial();

    QSerialPort *serial = nullptr;

signals:
    void signalUpdateData(QString coords, QString time);

private slots:
    void slotReceive();

public slots:
};

#endif // CNAVIPROVIDER_H
