#include "CNaviView.h"

CNaviView::CNaviView(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
}

void CNaviView::slotUpdateData(QString coords, QString time)
{
    this->coordLabel->setText(coords);
    this->timeLabel->setText(time);
}
