#include "CNaviController.h"

CNaviController::CNaviController()
{
    initModule();
}

CNaviController::~CNaviController()
{
    gpsReader->quit();
    gpsReader->wait();

}

EModule CNaviController::getType()
{
    return moduleType;
}

QWidget *CNaviController::getView()
{
    return view;
}

void CNaviController::initModule()
{
    view = new CNaviView();
    moduleType = EModule::Navi;
    navi = new CNaviProvider();
    gpsReader = new QThread();
    receiveTrigger = new QTimer();
    receiveTrigger->setInterval(1000);

    connect(gpsReader, SIGNAL(finished()), navi, SLOT(deleteLater()));
    connect(gpsReader, SIGNAL(finished()), receiveTrigger, SLOT(deleteLater()));
    connect(gpsReader, SIGNAL(started()), receiveTrigger, SLOT(start()));
    connect(receiveTrigger, SIGNAL(timeout()), navi, SLOT(slotReceive()));

    receiveTrigger->moveToThread(gpsReader);
    navi->moveToThread(gpsReader);
    gpsReader->start();
}
