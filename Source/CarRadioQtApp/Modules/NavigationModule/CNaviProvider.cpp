#include "CNaviProvider.h"
#include <QDebug>

CNaviProvider::CNaviProvider(QObject *parent) : QObject(parent)
{
    initNavi();
}

void CNaviProvider::initNavi()
{
    serial = new QSerialPort();
    connect(serial, SIGNAL(readyRead()), this, SLOT(slotReceive()));
    openSerial();
}

void CNaviProvider::openSerial()
{
    qDebug() << "Opening serial port";
    serial->setPortName("/dev/ttyS0");
    serial->setBaudRate(QSerialPort::Baud9600);
    serial->setDataBits(QSerialPort::Data8);
    serial->setParity(QSerialPort::NoParity);
    serial->setStopBits(QSerialPort::OneStop);
    serial->setFlowControl(QSerialPort::NoFlowControl);

    if(!serial->open(QIODevice::ReadWrite))
    {
        qDebug() << "Serial port is NOT OPEN";
        return;
    }
    qDebug() << "OPENED!";
}

void CNaviProvider::closeSerial()
{
    if(serial != nullptr)
    {
        if(serial->isOpen())
        {
            serial->close();
        }
        delete serial;
        serial = nullptr;
    }
}

void CNaviProvider::slotReceive()
{
    qDebug() << "Reading GPS data";
    QByteArray data;
    while((data.isNull() || data.isEmpty()) || !QString(data).contains('>'))
    {
        qDebug() << " Waiting for part of msg" ;
        //serial->waitForReadyRead();
        qDebug() << "Reading part of msg";
        data += serial->readAll();
        qDebug() << QString(data);
    }
}
