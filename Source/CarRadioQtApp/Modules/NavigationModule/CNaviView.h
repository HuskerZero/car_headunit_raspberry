#ifndef CNAVIVIEW_H
#define CNAVIVIEW_H

#include "ui_CNaviView.h"
#include <QString>

class CNaviView : public QWidget, private Ui::CNaviView
{
    Q_OBJECT

public:
    explicit CNaviView(QWidget *parent = 0);

public slots:
    void slotUpdateData(QString coords, QString time);
};

#endif // CNAVIVIEW_H
