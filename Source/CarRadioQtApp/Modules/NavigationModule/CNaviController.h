#ifndef CNAVICONTROLLER_H
#define CNAVICONTROLLER_H

#include <QObject>
#include "../MainModule/CBaseModule.h"
#include "../MainModule/EModule.h"
#include "CNaviView.h"
#include <QThread>
#include "CNaviProvider.h"
#include <QTimer>

class CNaviController : public CBaseModule
{
public:
    CNaviController();
    ~CNaviController();

private:
    QThread *gpsReader = nullptr;
    CNaviProvider *navi = nullptr;
    QTimer *receiveTrigger = nullptr;


public:
    virtual void initModule();
    virtual QWidget* getView();
    virtual EModule getType();
};

#endif // CNAVICONTROLLER_H
