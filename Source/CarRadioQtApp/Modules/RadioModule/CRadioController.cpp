#include "CRadioController.h"
#include "CRadioScreen.h"
#include <QThread>

CRadioController::CRadioController()
    : CBaseModule()
{
    initModule();
}

CRadioController::~CRadioController()
{
    radioThread->quit();
    radioThread->wait();

    if(nullptr != view)
    {
        delete view;
        view = nullptr;
    }

    if(nullptr != radioProvider)
    {
        delete radioProvider;
        radioProvider = nullptr;
    }
}


void CRadioController::initModule()
{
    moduleType = EModule::Radio;
    view = new CRadioScreen(nullptr);
    radioProvider = new CRadioProvider();
    radioThread = new QThread();

    connect(radioProvider, SIGNAL(signalCurrentFreqData(double, int)), view, SLOT(slotShowFreqData(double, int)));
    connect(view, SIGNAL(signalSearch(bool)), radioProvider, SLOT(slotSearch(bool)));
    //connect(radioThread, SIGNAL(started()), radioProvider, SLOT(start()));
    connect(radioThread, SIGNAL(finished()), radioProvider, SLOT(deleteLater()));

    radioProvider->moveToThread(radioThread);
    radioThread->start();
}


QWidget* CRadioController::getView()
{
    return view;
}


EModule CRadioController::getType()
{
    return moduleType;
}


