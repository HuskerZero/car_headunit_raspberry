#ifndef CRADIOPROVIDER_H
#define CRADIOPROVIDER_H

#include <iostream>
#include <wiringPiI2C.h>
#include <QObject>

#define ADDRESS_WRITE			0x60
#define ADDRESS_READ			0x61

/*	WRITE MODE	*/
/*	1ST BYTE	*/

#define MUTE					0x80
#define SEARCH					0x40

/*	3RD BYTE	*/

#define SEARCH_UP				0x80
#define SEARCH_STOP_LVL_LOW		0x20
#define SEARCH_STOP_LVL_MED		0x40
#define SEARCH_STOP_LVL_HI		0x60
#define HLSI					0x10

#define FORCE_MONO				0x08
#define MUTE_RIGHT				0x04
#define MUTE_LEFT				0x02
#define PORT1_ENABLE			0x01

/*	4TH BYTE	*/

#define PORT2_ENABLE			0x80
#define STANDBY_MODE			0x40
#define JPN_BAND				0x20
#define XTAL_FREQ				0x10

#define SOFT_MUTE				0x08
#define HIGH_CUT_ENABLE			0x04
#define NOISE_CANCEL_ENABLE		0x02
#define SEARCH_IND_ENABLE		0x01

#define PLLREF_FREQ				0x80
#define DTC						0x40

/*	READ MODE	*/
/*	1ST BYTe	*/

#define READY_FLAG				0x80
#define	BAND_LIMIT_REACHED		0x40

#define BAND_FREQ1				0x3F

/*	2ND BYTE	*/
#define BAND_FREQ2				0xFF

/*	3RD BYTE	*/
#define STEREO_MODE				0x80
#define IF_COUNTER				0x7F

/*	4TH BYTE	*/
#define ADC_LVL					0xF0

class CRadioProvider : public QObject
{
    Q_OBJECT

private:
    int device = 0;
    bool isReady = false;
    /* READ */

    double tunedFrequency = 0;
    int signalLevel = 0;
    bool stereo = false;

    /* WRITE */
    double desiredFrequency = 0;
    double minFrequency = 87.5;
    //double minFrequency = 92.0;
    double maxFrequency = 108;

    bool highSideInjection = true;
    bool mute = false;
    bool noiseCanceling = true;
    bool highCutControl = true;

    /* 32.768kHz */
    bool xtal = true;
    bool pllref = false;

public:
    CRadioProvider();
    ~CRadioProvider();
    void setFrequency(float mhz);
    void setHighSideInjection(bool enable);
    bool getHLSI();
    double getTunedFrequency();
    int getSignalLevel();
    bool isStereo();
    double getMinFreq();
    double getMaxFreq();

private:
    void encodeConfigAndSend();
    unsigned int encodePLL(double mhz);
    void sendCommand(unsigned char *msg);
    unsigned char* readData();
    void decodeData(unsigned char *msg);
    void setStereo(bool isOn);
    void setTunedFrequency(double mhz);
    void setSignalLevel(int lvl);

    bool getHighCutCtrl();
    bool isMute();
    bool getNoiseCanceling();
    bool getXtal();
    bool getPllRef();

    void showMsgOnTerminal(unsigned char *msg);

    double roundNumber(double a);

signals:
    void signalCurrentFreqData(double mhz, int signalStr);

public slots:
    void slotSearch(bool up);
};

#endif // CRADIOPROVIDER_H
