#ifndef CRADIOCONTROLLER_H
#define CRADIOCONTROLLER_H

#include "../MainModule/CBaseModule.h"
#include "CRadioProvider.h"

class CRadioController : public CBaseModule
{
    Q_OBJECT

public:
    CRadioController();
    ~CRadioController();

    // CBaseModule interface
public:
    virtual void initModule();
    virtual QWidget *getView();
    virtual EModule getType();

private:
    CRadioProvider *radioProvider = nullptr;
    QThread *radioThread = nullptr;

signals:
  //  void signalCurrentFreqData(double mhz, int signalStr);
   // void signalSearch(bool up);

public slots:
  //  void slotSearch(bool up);
};

#endif // CRADIOCONTROLLER_H
