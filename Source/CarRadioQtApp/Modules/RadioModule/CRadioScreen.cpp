#include "CRadioScreen.h"

CRadioScreen::CRadioScreen(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
}

void CRadioScreen::showFreqData(double mhz, int signalStr)
{
    slotShowFreqData(mhz, signalStr);
}

void CRadioScreen::slotShowFreqData(double mhz, int signalStr)
{
    this->lcdNumberFreq->display(mhz);
    this->signalStrengthBar->setValue(signalStr);
}

void CRadioScreen::on_pushButtonPreviousStation_clicked()
{
    emit signalSearch(false);
}

void CRadioScreen::on_pushButtonNextStation_clicked()
{
    emit signalSearch(true);
}
