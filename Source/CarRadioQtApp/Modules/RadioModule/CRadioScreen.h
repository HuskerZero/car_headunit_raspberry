#ifndef CRADIOSCREEN_H
#define CRADIOSCREEN_H

#include <QWidget>
#include <ui_CRadioScreen.h>

class CRadioScreen : public QWidget, private Ui::CRadioScreen
{
    Q_OBJECT

public:
    explicit CRadioScreen(QWidget *parent = 0);
    void showFreqData(double mhz, int signalStr);

public slots:
    void slotShowFreqData(double mhz, int signalStr);

signals:
    void signalSearch(bool up);

private slots:
    void on_pushButtonPreviousStation_clicked();
    void on_pushButtonNextStation_clicked();
};

#endif // CRADIOSCREEN_H
