#include "CRadioProvider.h"
#include <QThread>
//#include <cstdio>
#include <unistd.h>
#include <cmath>

CRadioProvider::CRadioProvider()
{
    device = wiringPiI2CSetup(ADDRESS_WRITE);
}

CRadioProvider::~CRadioProvider()
{
    close(device);
}

void CRadioProvider::setFrequency(float mhz)
{
    if(minFrequency <= mhz && mhz <= maxFrequency)
    {
        desiredFrequency = mhz;
        encodeConfigAndSend();
    }
}

void CRadioProvider::setHighSideInjection(bool enable)
{
    highSideInjection = enable;
}

double CRadioProvider::getTunedFrequency()
{
    return tunedFrequency;
}

int CRadioProvider::getSignalLevel()
{
    return (0 == signalLevel) ? 0 : signalLevel * 100 / 15;
}

bool CRadioProvider::isStereo()
{
    return stereo;
}

double CRadioProvider::getMinFreq()
{
    return minFrequency;
}

double CRadioProvider::getMaxFreq()
{
    return maxFrequency;
}

void CRadioProvider::encodeConfigAndSend()
{
    unsigned char *msg = new unsigned char[5];

    msg[0] = 0;
    msg[1] = 0;
    msg[2] = 0;
    msg[3] = 0;
    msg[4] = 0;

    std::cout << "Initial rx msg value" << std::endl;
    showMsgOnTerminal(msg);

    unsigned int freqPLL = encodePLL(desiredFrequency);
    std::cout << "PLL WORD full: " << std::hex << freqPLL << std::endl;
    unsigned char pll1 = (unsigned char)(freqPLL >> 8) & ~0xC0;
    unsigned char pll2 = (unsigned char)(freqPLL & 0xFF);

                            //first byte
    if(isMute())            msg[0] |= MUTE;
                            msg[0] |= pll1;

                            //second byte
                            msg[1] |= pll2;

                            //third byte
    if(getHLSI())           msg[2] |= HLSI;

                            //fourth byte
    if(getXtal())           msg[3] |= XTAL_FREQ;
    if(getHighCutCtrl())    msg[3] |= HIGH_CUT_ENABLE;
    if(getNoiseCanceling()) msg[3] |= NOISE_CANCEL_ENABLE;

                            //fifth byte
    if(getPllRef())         msg[4] |= PLLREF_FREQ;

    std::cout << "Configured rx msg value" << std::endl;
    showMsgOnTerminal(msg);
    sendCommand(msg);

    delete[] msg;

    QThread::msleep(300);
    decodeData(readData());
}

unsigned int CRadioProvider::encodePLL(double mhz)
{
    unsigned int pllWord = 0;

    if(getHLSI())
    {
        pllWord = (4*((mhz*1000000) + 225000)) / 32768;
    }
    else
    {
        pllWord = (4*((mhz*1000000) - 225000)) / 32768;
    }

    return pllWord;
}

void CRadioProvider::sendCommand(unsigned char *msg)
{
    unsigned char test[5] = {0};
    test[0] = msg[0];
    test[1] = msg[1];
    test[2] = msg[2];
    test[3] = msg[3];
    test[4] = msg[4];

    std::cout << "TEST: 0x" << std::hex << (unsigned int)test[0] << std::endl;

    //device = wiringPiI2CSetup(ADDRESS_WRITE);

    write(device, test, (isReady) ? 2 : 5);
    sync();
    isReady = true;

    //close(device);
}

unsigned char *CRadioProvider::readData()
{
    unsigned char test[5] = {0};

    //device = wiringPiI2CSetup(ADDRESS_WRITE);
    read(device, test, 4);
    //close(device);

    unsigned char *msg = new unsigned char[5];
    msg[0] = (unsigned char)test[0];
    msg[1] = (unsigned char)test[1];
    msg[2] = (unsigned char)test[2];
    msg[3] = (unsigned char)test[3];
    msg[4] = (unsigned char)test[4];

    std::cout << "Data from device:" << std::endl;
    showMsgOnTerminal(msg);
    return msg;
}

void CRadioProvider::decodeData(unsigned char *msg)
{
    unsigned int freq = 0;
    freq = ((((unsigned int)(msg[0] & ~0xC0)) << 8) & ~0xFF) | (unsigned int)msg[1];

    std::cout << "FREQ FROM tuner: 0x" << std::hex << freq << std::endl;

    if(getHLSI())
    {
        setTunedFrequency(roundNumber(((double)freq * (double)32768 / (double)4 - (double)225000) / (double)1000000));
    }
    else
    {
        setTunedFrequency(roundNumber(((double)freq * (double)32768 / (double)4 + (double)225000) / (double)1000000));
    }

    setStereo(msg[2] & STEREO_MODE);
    setSignalLevel((msg[3] & ADC_LVL) >> 4);
    delete[] msg;

}

void CRadioProvider::setStereo(bool isOn)
{
    stereo = isOn;
}

void CRadioProvider::setTunedFrequency(double mhz)
{
    std::cout << "tuned freq: " << std::dec << mhz << std::endl;
    tunedFrequency = mhz;
}

void CRadioProvider::setSignalLevel(int lvl)
{
    std::cout << "signal level: " << std::dec << lvl << std::endl;
    signalLevel = lvl;
}

bool CRadioProvider::getHLSI()
{
    return highSideInjection;
}

bool CRadioProvider::getHighCutCtrl()
{
    return highCutControl;
}

bool CRadioProvider::isMute()
{
    return mute;
}

bool CRadioProvider::getNoiseCanceling()
{
    return noiseCanceling;
}

bool CRadioProvider::getXtal()
{
    return xtal;
}

bool CRadioProvider::getPllRef()
{
    return pllref;
}

void CRadioProvider::showMsgOnTerminal(unsigned char *msg)
{
    std::cout
              << "byte1: 0x" << std::hex << (unsigned int)msg[0] << std::endl
              << "byte2: 0x" << std::hex << (unsigned int)msg[1] << std::endl
              << "byte3: 0x" << std::hex << (unsigned int)msg[2] << std::endl
              << "byte4: 0x" << std::hex << (unsigned int)msg[3] << std::endl
              << "byte5: 0x" << std::hex << (unsigned int)msg[4] << std::endl <<std::endl;

}

double CRadioProvider::roundNumber(double a)
{
    double  intpart;
    double fractpart = modf (a, &intpart);
    fractpart  = std::roundf(fractpart * 10.0)/10.0; // Round to 1 decimal place
    double b = intpart + fractpart;
    return b;
}

void CRadioProvider::slotSearch(bool up)
{
    double min = getMinFreq();
    double max = getMaxFreq();

    const int MIN_SIG_STR = 100;

    int counter = 0;
    const int MAX_ROUNDS = 3;
    bool stationFound = false;

    double currentFreq = (up) ? getTunedFrequency() + 0.1 : getTunedFrequency() - 0.1;

    if(currentFreq > max || currentFreq < min)
    {
        currentFreq = (up) ? min : max;
    }

    while(counter < MAX_ROUNDS)
    {
        if(up)
        {
            for(double i = currentFreq; i <= max; i+=0.1)
            {
                std::cout << "Wanted freq: " << i << std::endl;
                setFrequency(i);
                QThread::msleep(500);
                emit signalCurrentFreqData(i, getSignalLevel());

                if(getSignalLevel() >= MIN_SIG_STR)
                {
                    stationFound = true;
                    break;
                }
            }
            currentFreq = min;
        }
        else
        {
            for(double i = currentFreq; i >= min; i-=0.1)
            {
                std::cout << "Wanted freq: " << i << std::endl;
                setFrequency(i);
                QThread::msleep(500);
                emit signalCurrentFreqData(i, getSignalLevel());

                if(getSignalLevel() >= MIN_SIG_STR)
                {
                    stationFound = true;
                    break;
                }
            }
            currentFreq = max;
        }
        ++counter;

        if(stationFound)
        {
            break;
        }
        else
        {
            setHighSideInjection(!getHLSI());
        }
    }
}
