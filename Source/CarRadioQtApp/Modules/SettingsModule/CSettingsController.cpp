#include "CSettingsController.h"
#include "CSettingsView.h"

CSettingsController::CSettingsController()
{
    initModule();
}

CSettingsController::~CSettingsController()
{
    stopServer();
}

static const QLatin1String serviceUuid("e8e10f95-1a70-4b27-9ccf-0201903fc8ea");

void CSettingsController::startServer(const QBluetoothAddress &localAdapter)
{
    if (rfcommServer)
        return;

    //! [Create the server]
    rfcommServer = new QBluetoothServer(QBluetoothServiceInfo::RfcommProtocol, this);
    connect(rfcommServer, SIGNAL(newConnection()), this, SLOT(clientConnected()));
    bool result = rfcommServer->listen(localAdapter);
    if (!result)
    {
        qWarning() << "Nie mozna nasluchiwac na: " << localAdapter.toString();
        return;
    }

    QBluetoothServiceInfo::Sequence classId;

    classId << QVariant::fromValue(QBluetoothUuid(QBluetoothUuid::SerialPort));
    serviceInfo.setAttribute(QBluetoothServiceInfo::BluetoothProfileDescriptorList,
                             classId);

    classId.prepend(QVariant::fromValue(QBluetoothUuid(serviceUuid)));

    serviceInfo.setAttribute(QBluetoothServiceInfo::ServiceClassIds, classId);

    serviceInfo.setAttribute(QBluetoothServiceInfo::ServiceName, tr("BtRemoteRaspberry"));
    serviceInfo.setAttribute(QBluetoothServiceInfo::ServiceDescription,
                             tr("Server for raspberry remote"));
    serviceInfo.setAttribute(QBluetoothServiceInfo::ServiceProvider, tr("wi.zut.edu.pl"));

    serviceInfo.setServiceUuid(QBluetoothUuid(serviceUuid));

    QBluetoothServiceInfo::Sequence publicBrowse;
    publicBrowse << QVariant::fromValue(QBluetoothUuid(QBluetoothUuid::PublicBrowseGroup));
    serviceInfo.setAttribute(QBluetoothServiceInfo::BrowseGroupList,
                             publicBrowse);

    QBluetoothServiceInfo::Sequence protocolDescriptorList;
    QBluetoothServiceInfo::Sequence protocol;
    protocol << QVariant::fromValue(QBluetoothUuid(QBluetoothUuid::L2cap));
    protocolDescriptorList.append(QVariant::fromValue(protocol));
    protocol.clear();
    protocol << QVariant::fromValue(QBluetoothUuid(QBluetoothUuid::Rfcomm))
             << QVariant::fromValue(quint8(rfcommServer->serverPort()));
    protocolDescriptorList.append(QVariant::fromValue(protocol));
    serviceInfo.setAttribute(QBluetoothServiceInfo::ProtocolDescriptorList,
                             protocolDescriptorList);

    serviceInfo.registerService(localAdapter);

    qDebug() << "Serwer dziala!!!";
}

void CSettingsController::stopServer()
{
    serviceInfo.unregisterService();

    qDeleteAll(clientSockets);

    delete rfcommServer;
    rfcommServer = 0;
}

void CSettingsController::initModule()
{
    view = new CSettingsView();
    moduleType = EModule::Settings;
    localAdapters = QBluetoothLocalDevice::allDevices();
    QBluetoothLocalDevice adapter(localAdapters.at(0).address());
    adapter.setHostMode(QBluetoothLocalDevice::HostDiscoverable);

    connect(this, SIGNAL(signalClientConnected(QString)), view, SLOT(slotDeviceConnected(QString)));

    startServer();
}

QWidget *CSettingsController::getView()
{
    return view;
}

EModule CSettingsController::getType()
{
    return moduleType;
}

void CSettingsController::clientConnected()
{
    qDebug() << "Client connected";
    QBluetoothSocket *socket = rfcommServer->nextPendingConnection();

    if (nullptr == socket)
    {
        qDebug() << "OOps no socket";
        return;
    }

    clientSockets.append(socket);

    connect(socket, SIGNAL(readyRead()), this, SLOT(readSocket()));

    connect(socket, SIGNAL(disconnected()), this, SLOT(clientDisconnected()));
    qDebug() << "Udalo sie polaczyc";
    emit signalClientConnected(socket->peerName());
}

void CSettingsController::clientDisconnected()
{
    qDebug() << "Rozlaczylo";
    QBluetoothSocket *socket = qobject_cast<QBluetoothSocket *>(sender());
    if (!socket)
        return;

    emit signalClientConnected("NO_DEVICE");
    clientSockets.removeOne(socket);

    socket->deleteLater();
}

void CSettingsController::readSocket()
{
    QBluetoothSocket *socket = qobject_cast<QBluetoothSocket *>(sender());
    if (!socket)
        return;

    while (socket->canReadLine()) {
        QByteArray line = socket->readLine().trimmed();
        //emit messageReceived(socket->peerName(),
        //                     QString::fromUtf8(line.constData(), line.length()));
        qDebug() <<"Otrzymalem: " << QString::fromUtf8(line.constData());
        resolveCommand(QString::fromUtf8(line.constData()));
    }
}

void CSettingsController::resolveCommand(QString cmd)
{
    if(cmd.contains("switchDashboard")) emit signalSwitchModule(EModule::Dashboard);
    else if(cmd.contains("switchRecorder")) emit signalSwitchModule(EModule::Recorder);
    else if(cmd.contains("switchRadio")) emit signalSwitchModule(EModule::Radio);
    else if(cmd.contains("switchMusic")) emit signalSwitchModule(EModule::Player);
    else if(cmd.contains("switchSettings")) emit signalSwitchModule(EModule::Settings);
}


