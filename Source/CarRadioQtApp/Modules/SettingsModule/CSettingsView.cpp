#include "CSettingsView.h"

CSettingsView::CSettingsView(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
}

void CSettingsView::slotDeviceConnected(QString dev)
{
    this->connectedDeviceName->setText(dev);
}
