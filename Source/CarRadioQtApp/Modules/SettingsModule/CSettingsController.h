#ifndef CSETTINGSCONTROLLER_H
#define CSETTINGSCONTROLLER_H

#include <QObject>
#include "../MainModule/EModule.h"
#include "../MainModule/CBaseModule.h"
#include <QBluetoothServer>
#include <QBluetoothAddress>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QBluetoothLocalDevice>
#include <QBluetoothSocket>
#include <QBluetoothHostInfo>
#include <QBluetoothServiceInfo>
#include <QBluetoothUuid>

class CSettingsController : public CBaseModule
{
    Q_OBJECT
public:
    CSettingsController();
    ~CSettingsController();

    void startServer(const QBluetoothAddress &localAdapter = QBluetoothAddress());
    void stopServer();

public:
    virtual void initModule();
    virtual QWidget *getView();
    virtual EModule getType();

signals:
    void signalClientConnected(QString name);

private slots:
    void clientConnected();
    void clientDisconnected();
    void readSocket();

private:
    QBluetoothServer *rfcommServer = nullptr;
    QBluetoothServiceInfo serviceInfo;
    QList<QBluetoothSocket *> clientSockets;
    QList<QBluetoothHostInfo> localAdapters;


    void resolveCommand(QString cmd);
    //control signals

signals:
    void signalSwitchModule(EModule mod);
    void signalChangeStation();

};

#endif // CSETTINGSCONTROLLER_H
