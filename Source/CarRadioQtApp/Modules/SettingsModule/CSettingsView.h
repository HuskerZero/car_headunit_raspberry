#ifndef CSETTINGSVIEW_H
#define CSETTINGSVIEW_H

#include "ui_CSettingsView.h"

class CSettingsView : public QWidget, private Ui::CSettingsView
{
    Q_OBJECT

public:
    explicit CSettingsView(QWidget *parent = 0);

public slots:
    void slotDeviceConnected(QString dev);
};

#endif // CSETTINGSVIEW_H
