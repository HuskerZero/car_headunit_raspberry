#include "CDashboardScreen.h"

CDashboardScreen::CDashboardScreen(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
}

void CDashboardScreen::slotShowRPM(int rpm)
{
    this->displayRPM->display(rpm);
}
