#ifndef CDASHBOARDSCREEN_H
#define CDASHBOARDSCREEN_H

#include <QWidget>
#include "ui_CDashboardScreen.h"

class CDashboardScreen : public QWidget, private Ui::CDashboardScreen
{
    Q_OBJECT

public:
    explicit CDashboardScreen(QWidget *parent = 0);

signals:
    void signalUpdateRPM();

public slots:
    void slotShowRPM(int rpm);

};

#endif // CDASHBOARDSCREEN_H
