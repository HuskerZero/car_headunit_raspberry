#include "CDiagProvider.h"
#include <QDebug>
#include <iostream>
#include <string>
#include <QThread>

CDiagProvider::CDiagProvider()
{
    serial = new QSerialPort();
    initOBD();
}

CDiagProvider::~CDiagProvider()
{
    if(serial != nullptr)
    {
        if(serial->isOpen())
        {
            serial->close();
        }
        delete serial;
        serial = nullptr;
    }
}

void CDiagProvider::initOBD()
{
    openSerial();
}

void CDiagProvider::slotInitConnection()
{
    QByteArray data;
    while((data.isNull() || data.isEmpty()) || !QString(data).contains("ELM"))
    {
        QThread::msleep(300);
        send(QString("atz\r"));
        data = receive();
    }

    while((data.isNull() || data.isEmpty()) || !QString(data).contains("OK"))
    {
        QThread::msleep(300);
        send(QString("atsp0\r"));
        data = receive();
    }
    emit signalStartUpdating();
}

void CDiagProvider::openSerial()
{
    qDebug() << "Opening serial port";
    serial->setPortName("/dev/ttyUSB0");
    serial->setBaudRate(QSerialPort::Baud9600);
    serial->setDataBits(QSerialPort::Data8);
    serial->setParity(QSerialPort::NoParity);
    serial->setStopBits(QSerialPort::OneStop);
    serial->setFlowControl(QSerialPort::NoFlowControl);

    if(!serial->open(QIODevice::ReadWrite))
    {
        qDebug() << "Serial port is NOT OPEN";
        return;
    }
    qDebug() << "OPENED!";
}

void CDiagProvider::closeSerial()
{
    if(nullptr != serial && serial->isOpen())
    {
        serial->close();
    }
}

void CDiagProvider::send(QString data)
{
    if(serial->isOpen())
    {
        qDebug() << "Sending: " << data;
        serial->write(data.toLocal8Bit());
        serial->flush();
    }
}

QByteArray CDiagProvider::receive()
{
    QByteArray data;
    while((data.isNull() || data.isEmpty()) || !QString(data).contains('>'))
    {
        qDebug() << " Waiting for part of msg" ;
        //serial->waitForReadyRead();
        qDebug() << "Reading part of msg";
        data += serial->readAll();
    }
    return data;
}

HexData CDiagProvider::cleanHexData(QByteArray bytes)
{
    QString data(bytes);
    HexData hex;

    hex.data = nullptr;
    hex.numberOfBytes = 0;

    qDebug() << "Raw data: " << data;

    if(!data.contains("UNABLE", Qt::CaseInsensitive) && !data.contains("DATA", Qt::CaseInsensitive))
    {
        QString cleanStr = QString(bytes).remove("SEARCHING...", Qt::CaseInsensitive).remove('\r').remove(' ').remove('>');
        int nChars = cleanStr.length();

        qDebug() << "Clean data: " << cleanStr;

        QByteArray bArray(cleanStr.toLocal8Bit());
        unsigned char *returnBytes = new unsigned char[nChars/2];

        int counter = 0;

        for(int i = 0; i < nChars; i+=2)
        {
            returnBytes[counter++] = ((std::find(digits, digits + 16, bArray[i]) - digits) << 4) | (std::find(digits, digits + 16, bArray[i+1]) - digits);
        }
        hex.data = returnBytes;
        hex.numberOfBytes = nChars/2;

        std::cout << "Bytes chars: ";
        for(int i = 0; i < nChars/2; i++)
        {
            std::cout << std::hex << "0x" << (unsigned int)returnBytes[i] << std::endl;;
        }
    }

    return hex;
}

void CDiagProvider::updateSpeed()
{

}

void CDiagProvider::updateRPM()
{
    send(QString("010C\r"));
    HexData response = cleanHexData(receive());

    if(response.numberOfBytes > 4)
    {
        this->rpm = ((256 * (unsigned int)response.data[4]) + (unsigned int)response.data[5]) / 4;
    }
    else
    {
        this->rpm = 0;
    }

    emit signalRPM(this->rpm);

}

void CDiagProvider::updateOilTemp()
{

}

void CDiagProvider::updateCoolingTemp()
{

}

void CDiagProvider::updateThrothle()
{

}

void CDiagProvider::slotUpdateAll()
{
    qDebug() << "Apdejtuje";
    updateRPM();

}
