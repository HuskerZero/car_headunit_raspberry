#ifndef CDASHBOARDCONTROLLER_H
#define CDASHBOARDCONTROLLER_H

#include "../MainModule/CBaseModule.h"
#include "CDiagProvider.h"
#include <QThread>

class CDashboardController : public CBaseModule
{
public:
    CDashboardController();
    ~CDashboardController();
    // CBaseModule interface
public:
    virtual void initModule();
    virtual QWidget *getView();
    virtual EModule getType();

private:
    QThread *obdRunner = nullptr;
    CDiagProvider *obd = nullptr;

};

#endif // CDASHBOARDCONTROLLER_H
