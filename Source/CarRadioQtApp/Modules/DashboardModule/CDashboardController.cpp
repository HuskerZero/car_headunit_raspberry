#include "CDashboardController.h"
#include "CDashboardScreen.h"
#include <QTimer>

CDashboardController::CDashboardController()
    :CBaseModule()
{
    obd = new CDiagProvider();
    initModule();
}

CDashboardController::~CDashboardController()
{
    obdRunner->quit();
    obdRunner->wait();

    if(nullptr != view)
    {
        delete view;
        view = nullptr;
    }

    if(nullptr != obd)
    {
        delete obd;
        obd = nullptr;
    }
}

void CDashboardController::initModule()
{
    moduleType = EModule::Dashboard;
    view = new CDashboardScreen(nullptr);

    obdRunner = new QThread();
    QTimer *obdTrigger = new QTimer();
    obdTrigger->setInterval(500);
    //obd = new CDiagProvider();

    connect(view, SIGNAL(signalUpdateRPM()), obd, SLOT(slotUpdateAll()));
    connect(obd, SIGNAL(signalRPM(int)), view, SLOT(slotShowRPM(int)));
    connect(obdTrigger, SIGNAL(timeout()), obd, SLOT(slotUpdateAll()));
    connect(obdRunner, SIGNAL(finished()), obd, SLOT(deleteLater()));
    connect(obdRunner, SIGNAL(finished()), obdTrigger, SLOT(deleteLater()));
    connect(obd, SIGNAL(signalStartUpdating()), obdTrigger, SLOT(start()));
    //connect(obdRunner, SIGNAL(started()), obd, SLOT(slotInitConnection())); uncomment this for obd to work

    obdTrigger->moveToThread(obdRunner);
    obd->moveToThread(obdRunner);
    obdRunner->start();
}

QWidget *CDashboardController::getView()
{
    return view;
}

EModule CDashboardController::getType()
{
    return moduleType;
}
