#ifndef CDIAGPROVIDER_H
#define CDIAGPROVIDER_H

#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

struct HexData
{
    unsigned char *data;
    int numberOfBytes;
};

char const digits[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

class CDiagProvider : public QObject
{
    Q_OBJECT

public:
    CDiagProvider();
    ~CDiagProvider();

private:
    void initOBD();

    void openSerial();
    void closeSerial();
    void send(QString data);
    QByteArray receive();
    HexData cleanHexData(QByteArray bytes);

    void updateSpeed();
    void updateRPM();
    void updateOilTemp();
    void updateCoolingTemp();
    void updateThrothle();

private:
    QSerialPort *serial = nullptr;

    int speed = 0;
    int rpm = 0;
    int oilTemp = 0;
    int coolingTemp = 0;
    int throthle = 0;

signals:
    void signalRPM(int rpm);
    void signalStartUpdating();

public slots:
    void slotUpdateAll();
    void slotInitConnection();

};


#endif // CDIAGPROVIDER_H
