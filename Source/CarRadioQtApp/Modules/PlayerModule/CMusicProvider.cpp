#include "CMusicProvider.h"
#include <QDir>
#include <signal.h>

CMusicProvider::CMusicProvider()
{
    QDir directory("Music");
    songsAvailable = directory.entryList(QStringList() << "*.mp3",QDir::Files);
}

CMusicProvider::~CMusicProvider()
{
    if(nullptr != player)
    {
        if(QProcess::Running == player->state())
        {
            player->terminate();
            //player->terminate();
        }
        delete player;
        player = nullptr;
    }

}

void CMusicProvider::slotPlaySong(QString song)
{
    if(nullptr != player)
    {
        if(QProcess::Running == player->state())
        {
            player->terminate();
            delete player;
            player = nullptr;
            isRunning = false;
        }
    }

    currentSong = song;
    player = new QProcess();
    player->start("mpg123", QStringList() << "Music/" + song, QProcess::WriteOnly);
    isRunning = true;

    emit signalIsPlaying(isRunning);
    emit signalCurrentSong(song);
}

void CMusicProvider::slotPlayPause()
{
    if(nullptr != player)
    {
        if(QProcess::Running == player->state() && isRunning)
        {
            kill(player->processId(), SIGSTOP);
            isRunning = false;
        }
        else if(QProcess::Running != player->state() && isRunning)
        {
            isRunning = false;
            delete player;
        }
        else
        {
            kill(player->processId(), SIGCONT);
            isRunning = true;
        }
        emit signalIsPlaying(isRunning);
    }
}

void CMusicProvider::slotSendList()
{
    emit signalSongList(songsAvailable);
}
