#ifndef CPLAYERCONTROLLER_H
#define CPLAYERCONTROLLER_H

#include <QObject>
#include "../MainModule/CBaseModule.h"
#include "CMusicProvider.h"
#include <QThread>

class CPlayerController : public CBaseModule
{
public:
    CPlayerController();
    ~CPlayerController();

private:
    QThread *playerThread = nullptr;
    CMusicProvider * player = nullptr;

    virtual void initModule();
    virtual QWidget *getView();
    virtual EModule getType();

};

#endif // CPLAYERCONTROLLER_H
