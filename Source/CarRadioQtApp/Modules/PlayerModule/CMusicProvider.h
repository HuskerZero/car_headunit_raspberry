#ifndef CMUSICPROVIDER_H
#define CMUSICPROVIDER_H

#include <QObject>
#include <QProcess>

class CMusicProvider : public QObject
{
    Q_OBJECT

public:
    CMusicProvider();
    ~CMusicProvider();

private:
    bool isRunning = false;
    QString currentSong;
    QProcess *player = nullptr;
    QStringList songsAvailable;

signals:
    void signalSongList(QStringList list);
    void signalIsPlaying(bool yes);
    void signalCurrentSong(QString song);

public slots:
    void slotPlaySong(QString song);
    void slotPlayPause();
    void slotSendList();




};

#endif // CMUSICPROVIDER_H
