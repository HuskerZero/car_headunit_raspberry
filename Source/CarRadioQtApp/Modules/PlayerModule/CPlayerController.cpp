#include "CPlayerController.h"
#include "CPlayerScreen.h"

CPlayerController::CPlayerController()
{
    view = new CPlayerScreen();
    player = new CMusicProvider();
    initModule();
}

CPlayerController::~CPlayerController()
{
    playerThread->quit();
    playerThread->wait();

    if(nullptr != view)
    {
        delete view;
        view = nullptr;
    }
}

void CPlayerController::initModule()
{
    moduleType = EModule::Player;

    playerThread = new QThread();

    connect(playerThread, SIGNAL(finished()), player, SLOT(deleteLater()));
    connect(view, SIGNAL(signalPlaySong(QString)), player, SLOT(slotPlaySong(QString)));
    connect(view, SIGNAL(signalPlayPause()), player, SLOT(slotPlayPause()));
    connect(player, SIGNAL(signalSongList(QStringList)), view, SLOT(slotFillSongList(QStringList)));
    connect(player, SIGNAL(signalIsPlaying(bool)), view, SLOT(slotIsPlaying(bool)));
    connect(player, SIGNAL(signalCurrentSong(QString)), view, SLOT(slotMarkCurrent(QString)));
    connect(view, SIGNAL(signalRequestSongs()), player, SLOT(slotSendList()));

    player->moveToThread(playerThread);
    playerThread->start();

}

QWidget *CPlayerController::getView()
{
    return view;
}

EModule CPlayerController::getType()
{
    return moduleType;
}
