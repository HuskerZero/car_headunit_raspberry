#ifndef CPLAYERSCREEN_H
#define CPLAYERSCREEN_H

#include <QWidget>
#include "ui_CPlayerScreen.h"

class CPlayerScreen : public QWidget, private Ui::CPlayerScreen
{
    Q_OBJECT

public:
    explicit CPlayerScreen(QWidget *parent = 0);

signals:
    void signalPlaySong(QString song);
    void signalPlayPause();
    void signalRequestSongs();

public slots:
    void slotFillSongList(QStringList list);
    void slotIsPlaying(bool yes);
    void slotMarkCurrent(QString song);


private slots:
    void on_refreshListButton_clicked();
    void on_pushButtonPrevious_clicked();
    void on_pushButtonPlay_clicked();
    void on_pushButtonNext_clicked();
    void on_listSongs_itemClicked(QListWidgetItem *item);
};

#endif // CPLAYERSCREEN_H
