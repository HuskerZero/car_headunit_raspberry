#include "CPlayerScreen.h"

CPlayerScreen::CPlayerScreen(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
}

void CPlayerScreen::slotFillSongList(QStringList list)
{
    listSongs->clear();
    listSongs->addItems(list);
}

void CPlayerScreen::slotIsPlaying(bool yes)
{
    if(yes)
    {
        pushButtonPlay->setText("||");
    }
    else
    {
        pushButtonPlay->setText("►");
    }
}

void CPlayerScreen::slotMarkCurrent(QString song)
{
    QListWidgetItem *item = listSongs->currentItem();
    listSongs->selectAll();

    for(int i = 0; i < listSongs->selectedItems().count(); i++)
    {
        listSongs->selectedItems().at(i)->setBackgroundColor(Qt::white);
    }

    if(item->text().contains(song))
    {
        item->setBackgroundColor(Qt::green);
    }
}

void CPlayerScreen::on_refreshListButton_clicked()
{
    emit signalRequestSongs();
}

void CPlayerScreen::on_pushButtonPrevious_clicked()
{

}

void CPlayerScreen::on_pushButtonPlay_clicked()
{
    emit signalPlayPause();
}

void CPlayerScreen::on_pushButtonNext_clicked()
{

}

void CPlayerScreen::on_listSongs_itemClicked(QListWidgetItem *item)
{
    emit signalPlaySong(item->text());
}
