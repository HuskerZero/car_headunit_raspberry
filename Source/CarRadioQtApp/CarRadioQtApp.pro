#-------------------------------------------------
#
# Project created by QtCreator 2017-11-19T21:32:35
#
#-------------------------------------------------

QT       += core gui serialport bluetooth

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CarRadioQtApp
TEMPLATE = app

INCLUDEPATH += -I/home/marsfive/raspi/qt5pi/include
DEPENDPATH += /home/marsfive/raspi/qt5pi/include
LIBS += -L/home/marsfive/raspi/qt5pi/lib -lopencv_world
INCLUDEPATH += -I/home/marsfive/raspi/qt5pi/local/include
LIBS += -L/home/marsfive/raspi/qt5pi/local/lib -lwiringPi


SOURCES += Modules/MainModule/CMainScreen.cpp \
    Modules/MainModule/main.cpp \
    Modules/MainModule/CModuleManager.cpp \
    Modules/RadioModule/CRadioScreen.cpp \
    Modules/RecorderModule/CRecorderScreen.cpp \
    Modules/RadioModule/CRadioController.cpp \
    Modules/RecorderModule/CRecorderController.cpp \
    Modules/DashboardModule/CDashboardScreen.cpp \
    Modules/DashboardModule/CDashboardController.cpp \
    Modules/RecorderModule/CVideoProvider.cpp \
    Modules/RecorderModule/CVideoFileManager.cpp \
    Modules/DashboardModule/CDiagProvider.cpp \
    Modules/RadioModule/CRadioProvider.cpp \
    Modules/PlayerModule/CPlayerController.cpp \
    Modules/PlayerModule/CPlayerScreen.cpp \
    Modules/PlayerModule/CMusicProvider.cpp \
    Modules/SettingsModule/CSettingsController.cpp \
    Modules/SettingsModule/CSettingsView.cpp \
    Modules/NavigationModule/CNaviView.cpp \
    Modules/NavigationModule/CNaviController.cpp \
    Modules/NavigationModule/CNaviProvider.cpp

HEADERS  += Modules/MainModule/CMainScreen.h \
    Modules/MainModule/CModuleManager.h \
    Modules/MainModule/EModule.h \
    Modules/MainModule/CBaseModule.h \
    Modules/RadioModule/CRadioScreen.h \
    Modules/RecorderModule/CRecorderScreen.h \
    Modules/RadioModule/CRadioController.h \
    Modules/RecorderModule/CRecorderController.h \
    Modules/DashboardModule/CDashboardScreen.h \
    Modules/DashboardModule/CDashboardController.h \
    Modules/RecorderModule/CVideoProvider.h \
    Modules/RecorderModule/CVideoFileManager.h \
    Modules/DashboardModule/CDiagProvider.h \
    Modules/RadioModule/CRadioProvider.h \
    Modules/PlayerModule/CPlayerController.h \
    Modules/PlayerModule/CPlayerScreen.h \
    Modules/PlayerModule/CMusicProvider.h \
    Modules/SettingsModule/CSettingsController.h \
    Modules/SettingsModule/CSettingsView.h \
    Modules/NavigationModule/CNaviView.h \
    Modules/NavigationModule/CNaviController.h \
    Modules/NavigationModule/CNaviProvider.h

FORMS    += Modules/MainModule/CMainScreen.ui \
    Modules/RadioModule/CRadioScreen.ui \
    Modules/RecorderModule/CRecorderScreen.ui \
    Modules/DashboardModule/CDashboardScreen.ui \
    Modules/PlayerModule/CPlayerScreen.ui \
    Modules/SettingsModule/CSettingsView.ui \
    Modules/NavigationModule/CNaviView.ui



